#!/usr/bin/env bash

# Function to detect and return the package manager
detect_package_manager() {
    if command -v apt-get >/dev/null 2>&1; then
        echo "apt-get"
    elif command -v dnf >/dev/null 2>&1; then
        echo "dnf"
    elif command -v yum >/dev/null 2>&1; then
        echo "yum"
    elif command -v zypper >/dev/null 2>&1; then
        echo "zypper"
    elif command -v pacman >/dev/null 2>&1; then
        echo "pacman"
    else
        echo "Unsupported package manager. Please install dependencies manually."
        exit 1
    fi
}

pkg_manager=$(detect_package_manager)
echo "Detected package manager: $pkg_manager"

# Optionally, determine the Linux distribution and version
if command -v lsb_release >/dev/null 2>&1; then
    echo "Linux distribution and version: $(lsb_release -d -s)"
elif [[ -f /etc/os-release ]]; then
    . /etc/os-release
    echo "Linux distribution and version: $PRETTY_NAME"
else
    echo "Unable to determine Linux distribution and version."
fi
