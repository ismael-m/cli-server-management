#!/usr/bin/env zsh

# Destination host for the operation
DEST_HOST="$1"

# Function to ensure whiptail is installed
ensure_whiptail_installed() {
    if ssh "$DEST_HOST" which whiptail >/dev/null 2>&1; then
        echo "whiptail is already installed on $DEST_HOST."
    else
        echo "whiptail not found on $DEST_HOST. Attempting to install..."
        # Attempt to install whiptail. This uses apt-get for simplicity; adapt as necessary.
        ssh "$DEST_HOST" "sudo apt-get update && sudo apt-get install -y whiptail"
    fi
}

ensure_whiptail_installed
