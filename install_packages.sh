#!/usr/bin/env zsh

# Include the path to the ensure_whiptail.sh script
TOOLS_PATH="${TOOLS_PATH:-/tools}"
ENSURE_WHIPTAIL_SCRIPT="$TOOLS_PATH/ensure_whiptail.sh"
DEST_HOST="$1" # The target system

# Packages to consider for installation
declare -a packages=("curl" "vim" "htop")

# Function to install packages using an interactive selection
install_packages() {
    # Ensure whiptail is installed
    zsh "$ENSURE_WHIPTAIL_SCRIPT" "$DEST_HOST"
    
    # Generate options string for whiptail
    local options=()
    for pkg in "${packages[@]}"; do
        options+=("$pkg" "")
    done

    # Use whiptail on the remote system to select packages to install
    SELECTED_PACKAGES=$(ssh -t "$DEST_HOST" whiptail --title "Package Installation" --checklist \
    "Choose packages to install:" 20 78 10 "${options[@]}" 3>&1 1>&2 2>&3)

    if [ $? = 0 ]; then
        # Installing selected packages
        for pkg in $SELECTED_PACKAGES; do
            echo "Installing $pkg on $DEST_HOST..."
            ssh "$DEST_HOST" "sudo apt-get install -y $pkg"
        done
    else
        echo "No packages were selected for installation."
    fi
}

# Main execution
if [[ -z "$DEST_HOST" ]]; then
    echo "No destination host specified."
    exit 1
fi

install_packages
