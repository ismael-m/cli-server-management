#!/usr/bin/env zsh

# Configuration
TOOLS_PATH="${TOOLS_PATH:-/tools}"
GEN_COPY_SSH_KEY_SCRIPT="$TOOLS_PATH/gen_copy_ssh_key.sh"
INSTALL_PACKAGES_SCRIPT="$TOOLS_PATH/install_packages.sh"
# Destination host for SSH key copying and package installation
if [[ -z "$DEST_HOST" ]]; then
    echo "Enter the destination host (user@hostname):"
    read DEST_HOST
fi

# Call gen_copy_ssh_key.sh with any necessary arguments
if [[ -f "$GEN_COPY_SSH_KEY_SCRIPT" ]]; then
    echo "Running SSH key generation and copying..."
    zsh "$GEN_COPY_SSH_KEY_SCRIPT" --dest "$DEST_HOST"
else
    echo "SSH key script not found: $GEN_COPY_SSH_KEY_SCRIPT"
    exit 1
fi

# Proceed to install packages on the target system
if [[ -f "$INSTALL_PACKAGES_SCRIPT" ]]; then
    echo "Proceeding to install packages on the target system..."
    zsh "$INSTALL_PACKAGES_SCRIPT" --dest "$DEST_HOST"
else
    echo "Install packages script not found: $INSTALL_PACKAGES_SCRIPT"
    exit 1
fi
