#!/usr/bin/env zsh

# Function to display help
show_help() {
    echo "Usage: $0 [options]"
    echo ""
    echo "This script generates an ED25519 SSH key, copies it to a remote server,"
    echo "and optionally updates the user's .ssh/config."
    echo ""
    echo "Options:"
    echo "  -d, --dest HOST_URI             Specify the destination host URI directly."
    echo "  -s, --server-name NAME          Specify the name for the new server in the .ssh/config."
    echo "  -u, --user USERNAME             Specify the username for the SSH config entry."
    echo "  -h, --help                      Show this help message and exit."
    echo ""
    echo "If no option is provided, the script will prompt for necessary information."
}

# Function to detect the package manager and install ssh-copy-id if needed
ensure_ssh_copy_id_installed() {
    if ! command -v ssh-copy-id &> /dev/null; then
        echo "ssh-copy-id not found. Attempting to install..."
        local pkg_manager
        if command -v apt-get &> /dev/null; then
            pkg_manager="apt-get"
        elif command -v dnf &> /dev/null; then
            pkg_manager="dnf"
        elif command -v yum &> /dev/null; then
            pkg_manager="yum"
        elif command -v zypper &> /dev/null; then
            pkg_manager="zypper"
        elif command -v pacman &> /dev/null; then
            pkg_manager="pacman"
        else
            echo "Unsupported package manager. Please install ssh-copy-id manually."
            exit 1
        fi

        echo "Using package manager: $pkg_manager"

        case $pkg_manager in
            apt-get)
                sudo apt-get update && sudo apt-get install -y openssh-client
                ;;
            dnf|yum)
                sudo $pkg_manager install -y openssh-clients
                ;;
            zypper)
                sudo zypper install -y openssh-clients
                ;;
            pacman)
                sudo pacman -Sy openssh
                ;;
        esac
    fi
}

# Function to generate and copy SSH key, and update .ssh/config if needed
generate_and_copy_ssh_key() {
    local dest_host=$1
    local server_name=$2
    local username=$3

    # Generate the ED25519 key if not already existing
    local ssh_key_path="$HOME/.ssh/id_ed25519"
    if [[ ! -f "$ssh_key_path" ]]; then
        echo "Generating an ED25519 SSH key at $ssh_key_path..."
        ssh-keygen -t ed25519 -f "$ssh_key_path" -C "" -N ""
    else
        echo "SSH key already exists at $ssh_key_path"
    fi

    # Copy the SSH key to the remote server
    if [[ -n "$dest_host" ]]; then
        echo "Copying the SSH key to $dest_host..."
        ssh-copy-id -i "${ssh_key_path}.pub" "$dest_host"
    else
        echo "Destination host not specified. Skipping key copy."
    fi

    # Update .ssh/config if server name and username are provided
    if [[ -n "$server_name" && -n "$username" ]]; then
        echo "Updating .ssh/config with server $server_name"
        {
            echo "\nHost $server_name"
            echo "  HostName $dest_host"
            echo "  User $username"
            echo "  IdentityFile $ssh_key_path"
        } >> "$HOME/.ssh/config"
        echo ".ssh/config updated."
    fi
}

# Main script
DEST_HOST=""
SERVER_NAME=""
USERNAME=""

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -d|--dest) DEST_HOST="$2"; shift ;;
        -s|--server-name) SERVER_NAME="$2"; shift ;;
        -u|--user) USERNAME="$2"; shift ;;
        -h|--help) show_help; exit 0 ;;
        *) echo "Unknown option: $1" >&2; exit 1 ;;
    esac
    shift
done

ensure_ssh_copy_id_installed

if [[ -z "$DEST_HOST" ]]; then
    echo "Enter the destination host (user@hostname):"
    read DEST_HOST
fi
if [[ -z "$SERVER_NAME" ]]; then
    echo "Enter the server name for .ssh/config (leave blank to skip):"
    read SERVER_NAME
fi
if [[ -z "$USERNAME" ]]; then
    echo "Enter the username for the SSH config entry (leave blank to skip):"
    read USERNAME
fi

generate_and_copy_ssh_key "$DEST_HOST" "$SERVER_NAME" "$USERNAME"