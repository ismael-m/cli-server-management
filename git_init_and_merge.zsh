#!/bin/zsh

# Default tools path
TOOLS_PATH="${TOOLS_PATH:-/tools}"

# Help function
show_help() {
    echo "Usage: $0 [-h | --help] [--repo=<repository-url>]"
    echo ""
    echo "Options:"
    echo "  -h, --help            Show this help message and exit."
    echo "  --repo=<repository-url>  Specify the repository URL. Overrides \$TOOLS_REPO."
}

# Parse command line arguments
for arg in "$@"; do
    case $arg in
        -h|--help)
            show_help
            exit 0
            ;;
        --repo=*)
            TOOLS_REPO="${arg#*=}"
            shift
            ;;
        *)
            ;;
    esac
done

# Check if TOOLS_REPO is set or not
if [ -z "${TOOLS_REPO}" ]; then
    echo -n "Enter the repository URL: "
    read TOOLS_REPO
    if [ -z "${TOOLS_REPO}" ]; then
        echo "Repository URL cannot be empty."
        exit 1
    fi
fi

# Include other scripts
source "${TOOLS_PATH}/git_verify_init.zsh"
source "${TOOLS_PATH}/git_add_commit.zsh"
source "${TOOLS_PATH}/git_merge_remote.zsh"

# Run the scripts
git_verify_init
git_add_commit "Initial commit"
git_merge_remote "${TOOLS_REPO}"

