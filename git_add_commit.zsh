#!/bin/zsh

git_add_commit() {
    local commit_message="$1"
    git add .
    git commit -m "${commit_message}"
}
