#!/bin/zsh

git_verify_init() {
    if [ ! -d ".git" ]; then
        echo "Initializing a new git repository..."
        git init
    else
        echo "Git repository already exists."
    fi
}
