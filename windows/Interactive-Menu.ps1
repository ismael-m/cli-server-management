# Interactive-Menu.ps1

param (
    [string]$ConfigFile = "Config.yaml"
)

# Source the configuration
. .\Config.ps1 -ConfigFile $ConfigFile

function Show-Menu {
    Write-Host "Welcome to the Interactive CLI Menu" -ForegroundColor Green
    Write-Host "Choose an option:"

    # Loop through menu items
    foreach ($menu in $Config.menus) {
        Write-Host "[$($menu.type.ToUpper()) - $($menu.id)]: Placeholder Option"
        # You can replace "Placeholder Option" with your actual menu options
    }
}

Show-Menu
