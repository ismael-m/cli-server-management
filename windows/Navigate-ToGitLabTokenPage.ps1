# Navigate-ToGitLabTokenPage.ps1

param (
    [string]$test = $null
)

function Open-GitLabTokenPage {
    $gitLabTokenUrl = "https://gitlab.com/-/profile/personal_access_tokens"
    Start-Process $gitLabTokenUrl -NoNewWindow -PassThru | Out-Null
}

Open-GitLabTokenPage
