param (
    [string]$ConfigFile = "config.yaml"
)

function Get-ScriptName {
    $scriptName = $MyInvocation.InvocationName
    $scriptNameWithoutExtension = [System.IO.Path]::GetFileNameWithoutExtension($scriptName)
    return $scriptNameWithoutExtension
}

$ScriptName = Get-ScriptName

function Get-Config {
    [CmdletBinding()]
    param (
        [string]$ConfigFile,
        [string]$ScriptName
    )

    if (-not (Test-Path $ConfigFile)) {
        Write-Error "Config file '$ConfigFile' not found."
        exit 1
    }

    try {
        $config = Get-Content $ConfigFile -Raw | ConvertFrom-Yaml
        if (-not $config.$ScriptName) {
            Write-Error "Section for script '$ScriptName' not found in the config file."
            exit 1
        }
        return $config.$ScriptName
    } catch {
        Write-Error "Failed to parse config file. Please ensure it's in proper YAML format."
        exit 1
    }
}

$Config = Get-Config -ConfigFile $ConfigFile -ScriptName $ScriptName
