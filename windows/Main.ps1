# Main.ps1

param (
    [string]$ConfigFile = "config.yaml"
)

# Source the configuration
. .\Config.ps1 -ConfigFile $ConfigFile

# Function to show the interactive menu
function Show-Menu {
    Write-Host "Welcome to the Interactive CLI Menu" -ForegroundColor Green
    Write-Host "Choose an option:"

    # Loop through menu items
    foreach ($menu in $Config.menus) {
        Write-Host "[$($menu.type.ToUpper()) - $($menu.id)]: $($menu.value)"
        # You can replace "Placeholder Option" with your actual menu options
    }
}

# Show the menu
Show-Menu
