# Install-Dependency.ps1

param (
    [string]$test = $null
)

# Function to install Chocolatey
function Install-Chocolatey {
    Write-Host "Installing Chocolatey..."
    Set-ExecutionPolicy Bypass -Scope Process -Force
    iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

# Function to install GPG
function Install-GPG {
    if (-not (Get-Command gpg -ErrorAction SilentlyContinue)) {
        if (Get-Command winget -ErrorAction SilentlyContinue) {
            winget install GnuPG.GnuPG
        }
        elseif (Get-Command choco -ErrorAction SilentlyContinue) {
            choco install gnupg -y
        }
        else {
            Install-Chocolatey
            choco install gnupg -y
        }
    }
}

Install-GPG
