# Encrypt-Token.ps1

param (
    [string]$ConfigFile = "config.yaml",
    [string]$test = $null
)

# Source the configuration
. .\Config.ps1 -ConfigFile $ConfigFile

function Encrypt-GitLabToken {
    $token = Read-Host "Please enter your GitLab application token"
    $gpgRecipient = $Config.gpgRecipientEmail
    if ([string]::IsNullOrWhiteSpace($gpgRecipient)) {
        $gpgRecipient = Read-Host "Enter the GPG recipient email"
    }
    $encryptedToken = echo $token | gpg --encrypt --armor --recipient $gpgRecipient
    $encryptedToken | Out-File "encrypted_gitlab_token.asc"
    Write-Host "Your token has been encrypted and saved to encrypted_gitlab_token.asc"
}

Encrypt-GitLabToken
