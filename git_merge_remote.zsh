#!/bin/zsh

git_merge_remote() {
    local repo_url="$1"
    git remote add origin "${repo_url}" || git remote set-url origin "${repo_url}"
    git fetch origin
    git branch -M main
    git pull origin main --allow-unrelated-histories || echo "Merge required manually."
    git push -u origin main
}
